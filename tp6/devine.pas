program devine;
uses crt;
var x,n,nbr,essais : integer;
begin
  essais:=1;
  writeln('Rentrez un nombre � faire deviner');
  readln(x);
  clrscr;
  repeat
    writeln('Rentrez un nombre de tentatives');
    readln(n);
    clrscr;
    if (n<=0) then
        writeln('rentrez un nombre sup�rieur � 0');
  until (n>0);
  while (n<>0) or (nbr<>x) do
    begin
        writeln('Devinez le nombre myst�re. Il vous reste ', n,' tentatives :');
        readln(nbr);
        if (nbr<x) then
            writeln('Trop petit');
        if (nbr>x) then
            writeln('Trop grand');
        n:=n-1;
        if (nbr<>x) then
            essais:=essais+1;
        
    end;
  if (nbr=x) then
    writeln('Bravo ! Vous avez devin� en ', essais,' coups!')
  else
    writeln('D�sol�, vous avez perdu');
  readln();
end.
