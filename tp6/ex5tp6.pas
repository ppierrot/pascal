program td6ex5;
uses crt;
const n=150;
type tableau=array [1..n] of integer;
var tab : tableau;
    i,min,pos,m,stock : integer;
begin
    randomize;
    pos:=1;
    tab[1]:=random(51);
    min:=tab[1];
    for i:=2 to n do
        begin
            tab[i]:=random(51);
            if (tab[i]<min) then
                begin
                    min:=tab[i];
                    pos:=i;
                    stock:=tab[2];
                    tab[2]:=tab[i];
                    tab[i]:=stock;
                end
        end;
    m:=0;
    for i:=1 to i do
        begin
            if (m=10) then
                begin
                    writeln();
                    m:=0;
                end;
            write(' ', tab[i], ' ');
            m:=m+1;
        end;
    writeln();
    writeln('la plus petite valeur est ', min, ' de position initiale ', pos);
    readln();
end.
