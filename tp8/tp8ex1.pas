program tp8ex1;
type coordonnées = record
     x : integer;
     y : integer;
     end;
var p,o : coordonnées;
    dist : real;
begin
    writeln('Veuillez rentrer la coordonnée x du point');
    readln(p.x);
    writeln('Veuillez rentrer la coordonnée y du point');
    readln(p.y);
    o.x:=0;
    o.y:=0;
    dist:= sqrt(((p.x-o.x)*(p.x-o.x))+((p.y-o.y)*(p.y-o.y)));
    writeln(dist:3:2);
    readln();
end.
