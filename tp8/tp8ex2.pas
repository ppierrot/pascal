program tp8ex2;
type points = record
     x : real;
     y : real;
     end;
     c�t�s = record
     ab : real;
     bc : real;
     ac : real;
     end;
var  a,b,c : points;
     dist : c�t�s;
     hypo,long1,long2 : real;
     pts,cor : char;
     j,i : integer;
begin
    {on demande de rentrer les coordonn�es des points respectif}
    for j:=1 to 3 do
        begin
            i:=1;
            case j of
                1 : pts:='a';
                2 : pts:='b';
                3 : pts:='c';
            end;
            for i:=1 to 2 do
                begin
                    case i of
                        1 : cor:='x';
                        2 : cor:='y';
                    end;
                    writeln('Veuillez rentrer la coordonn�e ',cor,' du point ', pts);
                    if (i=1) then
                        begin
                            case j of
                                1 : readln(a.x);
                                2 : readln(b.x);
                                3 : readln(c.x);
                            end;
                        end
                    else
                        begin
                            case j of
                                1 : readln(a.y);
                                2 : readln(b.y);
                                3 : readln(c.y);
                            end;
                        end
                end;
        end;

    {on d�finit la longeur ab en calculant la distance entre le point a et b}
    dist.ab:= (b.x-a.x)*(b.x-a.x)+(b.y-a.y)*(b.y-a.y);
    hypo:=dist.ab; {on enregistre ce c�t� comme �tant l'hypoth�nuse, pour l'instant}
    
    {on d�finit bc}
    dist.bc:= (c.x-b.x)*(c.x-b.x)+(c.y-b.y)*(c.y-b.y);
    long1:=dist.bc; {on enregistre le c�t� dans une variable autre que hypo}
    if (dist.bc>hypo) then {si bc est plus grand, alors il devient l'hypoth�nuse}
        begin
            hypo:=dist.bc;
            long1:=dist.ab;
        end;
        
    {on d�finit ac}
    dist.ac:= (c.x-a.x)*(c.x-a.x)+(c.y-a.y)*(c.y-a.y);
    long2:=dist.ac;
    if (dist.ac>hypo) then {on v�rifie l'hypoth�nuse}
        begin
            hypo:=dist.ac;
            long1:=dist.ab;
            long2:=dist.bc;
        end;

    {on v�rifie tous les c�t�s sont sup�rieurs � 0, sinon �a ne formerait pas un triangle}
    if (dist.bc>0) and (dist.ac>0) and (dist.ab>0) then
        begin
            {on v�rifie si les c�t�s sont �quilat�ral}
            if (dist.ab=dist.bc) and (dist.bc=dist.ac) then
                writeln('c''est �quilat�ral')
            else
                {si c'est pas �quilat�ral, alors on v�rifie si c'est isoc�le}
                if (dist.ab=dist.bc) or (dist.bc=dist.ac) or (dist.ab=dist.ac) then
                    writeln('c''est isoc�le');

            {on v�rifie si c'est triangle rectangle avec formule pythagore : a^2 + b^2 = c^2}
            if ((hypo-long1+long2)<0.01) then
                writeln('le triangle est rectangle');
        end
    else
        begin
            writeln('Erreur : un ou plusieurs c�t�s sont nuls.');
            writeln(dist.ab:3:2);
            writeln(dist.bc:3:2);
            writeln(dist.ac:3:2);
        end;
    readln();
end.
