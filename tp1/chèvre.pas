program ch�vre;
var lo,la,R,A,p,cout:real;
const jardin = 100;
 begin
  writeln('Bonjour, nous allons calculer la surface d''herbe de la ch�vre (', jardin, ' m^2) de Mr.Seguin apr�s construction de son abri.');
  writeln('Rentrez la longueur de l''abri en m�tres :');
  readln(lo);
  writeln('Maintenant, rentrez sa largeur :');
  readln(la);
  R:= jardin/(2*3.14);
  A:= jardin-(lo*la);
  writeln('Avec l''abri, il restera ', A:3{chiffres apr�s virugle}:0{chiffres avant virgule},' m^2 � la ch�vre.');
  writeln('Maintenant, nous allons simuler le co�t pour l''installation d''une cl�ture autour du terrain.');
  writeln('Veuillez rentrer le prix en euros au m�tre :');
  readln(p);
  cout:= p*(2*3.14*R);
  writeln('La cl�ture reviendra � ', cout:3:2,' euros.') ;
  readln();
 end.
