PROGRAM Exempleb ;
    Uses Crt ;
    PROCEDURE Saisie ( VAR nom : String ) ;
    BEGIN
        Write('Entrez votre nom : ') ;
        ReadLn(nom) ;
    END ;
    PROCEDURE Affichage (  info : String ) ;
    BEGIN
        WriteLn('BONJOUR ', info, ' COMMENT ALLEZ VOUS?') ;
    END ;
    {D�claration des variables du programme principal}
    VAR chaine : String ;
    BEGIN
        ClrScr ;
        Saisie(chaine) ;
        Affichage(chaine) ;
        readln;
END.

{sans le VAR devant le Saisie, le nom que l'on rentre ne s'affiche pas quand le programme nous demande si l'on va bien.}
{info ne fait que afficher contrairement � nom qui enregistre une valeur}
{il ne compilera pas car ce n'est pas une valeur}
