program ex4;

const n=6;

type matrice = array[1..n,1..n] of integer;

procedure calcul (var triang : matrice);
var i,j : integer;
    cond : boolean;
begin
    for j:=1 to n do
        for i:=1 to n do
            begin
                if (j>i) and (i>1) then
                    begin
                        triang[j,i]:=triang[j-1,i] + triang[j-1,i-1];
                    end;
                if (i>=1) then
                    begin
                        triang[1,1]:=1;
                        triang[i,1]:=1;
                        triang[i,i]:=1;
                    end;
            end;
end;

procedure affichage (triang : matrice);
var i,j,m : integer;
begin
    m:=0;
    for j:=1 to n do
        for i:=1 to n do
            begin
                if (m=n) then
                begin
                    writeln();
                    m:=0;
                end;
                if (triang[j,i]<>0) then
                    write('  ', triang[j,i], '  ');
                m:=m+1;
            end;
end;

var  tripascal : matrice ;
begin
    calcul(tripascal);
    affichage(tripascal);
    readln;
end.


        


