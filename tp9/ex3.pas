program ex3;

type schtroumpf = record
     nom : string;
     genre : char;
     couleur : string;
     end;
     
procedure gargamel (var cible : schtroumpf ; couleurpotion : string);
begin
    if (cible.nom='Schtroumpfette') then
        cible.genre:='f'
    else
        cible.genre:='m';
    writeln('Attaque potion ',couleurpotion,' sur ',cible.nom,' !');
    if (cible.genre='m') then
        cible.couleur:=couleurpotion
    else
        cible.couleur:='bleu';
end;

procedure affiche(cible : schtroumpf);
begin
    Writeln(cible.nom,': genre= ', cible.genre,', couleur= ', cible.couleur);
end;
var couleur : string;
    cible : schtroumpf;
begin
    writeln('Nom du schtroumpf');
    readln(cible.nom);
    writeln('Couleur de la potion');
    readln(couleur);
    gargamel(cible,couleur);
    affiche(cible);
    readln;
end.
