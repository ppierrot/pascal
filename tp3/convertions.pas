program conversion;
var valeur, valconv : real;
    unite : char;
begin
    writeln('Rentrez la valeur que vous voulez convertir');
    readln(valeur);
    writeln('Quelle unit� voulez-vous convertir ? m (m�trique), g (gramme), c (degr� celcius) ?');
    readln(unite);
    case unite of
        'm' : begin
                valconv:= valeur/0.32808;
                writeln(valeur:5:2,'m�tres = ', valconv:5:2, ' pieds !');
            end;
        'g' : begin
                valconv:= valeur*0.002205;
                writeln(valeur:5:2,'grammes = ', valconv:5:2, ' livres !');
            end;
        'c' : begin
                valconv:= 32+(1.8*valeur);
                writeln(valeur:5:2,'degr�s = ', valconv:5:2, ' farenheit !');
            end;
        else writeln('Unit� incorrecte');
    end;
 Readln();
end.
