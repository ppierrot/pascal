program ex4;
uses crt;
const V=5; M=4;
type tableauDventes = array[1..V,1..M] of integer;
     tab1Dmodeles = array[1..M]of integer;
var i,j,s,total: integer;
    tabdv : tableauDventes;
    somv,prix : tab1Dmodeles;
begin
    s:=0;
    randomize;
    for i:=1 to V do
        for j:=1 to M do
            begin
                tabdv[i,j]:=random(10); {Les valeurs sont attribu�es au hasard au lieu d'�tre rentr�es une par une, question de commodit�}
                somv[j]:=somv[j]+tabdv[i,j];
            end;
     for i:=1 to V do
        for j:=1 to M do
            begin
                if (s=M) then
                    begin
                        writeln();
                        s:=0;
                    end;
                write('  ', tabdv[i,j],'  ');
                s:=s+1;
            end;
    writeln();
    for j:=1 to M do
            begin
                writeln('Rentrez le prix de vente pour le mod�le ',j);
                readln(prix[j]);
                total:=total+somv[j]*prix[j];
            end;
    writeln();
    for j:=1 to M do
        writeln(somv[j],' �xemplaires du mod�le ',j,' ont �t� vendus.');
    writeln();
    writeln('La recette des ventes s''�l�ve � ',total,' euros.');
    readln();
end.
