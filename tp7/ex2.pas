program tp7ex2;
uses crt;
const nblig=5;
      nbcol=10;
type tableau = array [1..nblig, 1..nbcol] of integer;
var tab : tableau;
    i,j,nbr9,m : integer;
    switch : boolean;
begin
    nbr9:=0;
    m:=0;
    randomize;
        for i:=1 to nblig do
            for j:=1 to nbcol do
             begin
                tab[i,j]:=random(21); {Les valeurs sont attribu�es au hasard au lieu d'�tre rentr�es une par une, question de commodit�}
                if (m=nbcol) then
                    begin
                        writeln();
                        m:=0;
                    end;
                write(' ', tab[i,j], ' ');
                m:=m+1;
             end;
    writeln();
        for i:=1 to nblig do
            begin
                for j:=1 to nbcol do
                    begin
                        if (tab[i,j]=9) then
                            nbr9:=nbr9+1;
                    end;
                writeln('la valeur 9 appara�t ',nbr9,' fois dans la ligne n�',i);
                nbr9:=0;
            end;
        nbr9:=0;
        for j:=1 to nbcol do
          begin
                 for i:=1 to nblig do
                    begin
                        if (tab[i,j]=9) then
                        nbr9:=nbr9+1;
                    end;
                 writeln('la valeur 9 appara�t ',nbr9,' fois dans la colonne n�',j);
                 nbr9:=0;
          end;
    readln();
end.
